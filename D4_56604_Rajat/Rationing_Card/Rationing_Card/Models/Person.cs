﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rationing_Card.Models
{
    public class Person
    {
        
        public Person(int RationNo, string PersonName, int Age, string Address, string  ContactNumber, string Status, string Role)
        {
            this.PersonName = PersonName;
            this.Address = Address;
            this.ContactNumber = ContactNumber;
            this.Status = Status;
            this.Role = Role;
            this.RationNo = RationNo;
            this.Age = Age;

        }

        public Person()
        {

        }
        public int RationNo { get; set; }
        public string PersonName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Status { get; set; }
        public string Role { get; set; }
    }
}