﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rationing_Card.Models
{
    public class Credentials
    {
        public string PersonName { get; set; }
        public int RationNo { get; set; }
    }
}