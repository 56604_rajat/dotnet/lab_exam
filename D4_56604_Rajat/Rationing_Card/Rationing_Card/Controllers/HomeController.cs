﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rationing_Card.Models;

namespace Rationing_Card.Controllers
{
    public class HomeController : Controller
    {
        RationEntities rationobj = new RationEntities();
        // GET: Home
        public ActionResult Login()
        {
            return View("Login");
        }

        public ActionResult AfterLogin(Credentials cred)
        {
            RationCardTB rationHolder = rationobj.RationCardTBs.Find(cred.RationNo);
            ViewBag.message = "Invalid Credentials";
            if (rationHolder.PersonName== cred.PersonName)
            {
                if(rationHolder.Role == "Admin")
                {
                    return Redirect("/Home/AdminHome");
                }
            }

            return View("Login");
        }

        public ActionResult AdminHome()
        {
            return View("AdminHome",rationobj.RationCardTBs.ToList());
        }

        public ActionResult Approve(int id)
        {
            RationCardTB rationHolder = rationobj.RationCardTBs.Find(id);
            rationHolder.Status = "Approved";
            rationobj.SaveChanges();
            return Redirect("/Home/AdminHome");
        }

        public ActionResult Add()
        {
            return View("Add");
        }

        public ActionResult AfterAdd(RationCardTB rationHolder)
        {
            rationobj.RationCardTBs.Add(rationHolder);
            rationobj.SaveChanges();
            return Redirect("/Home/AdminHome");
        }

        
    }
}